// Return customers who have more undelivered orders than delivered
fun Shop.getCustomersWithMoreUndeliveredOrders(): Set<Customer> 
    { 
        var res: MutableList<Customer> = mutableListOf()
    	for(customer in customers)
        {
            val (delivered, undelivered) = customer.orders.partition {it.isDelivered}
            if(undelivered.size > delivered.size) res.add(customer)
        }
        return res.toSet()
    }
