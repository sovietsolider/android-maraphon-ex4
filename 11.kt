fun Shop.getProductsOrderedByAll(): Set<Product> 
{
    return customers.fold( getAllOrderedProducts(), {orderedByAll, customer -> orderedByAll.intersect(customer.getOrderedProducts())})
}
fun Shop.getAllOrderedProducts(): Set<Product> = customers.flatMap { it.getOrderedProducts() }.toSet()

fun Customer.getOrderedProducts(): List<Product> =
        orders.flatMap{it.products}
